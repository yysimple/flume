package com.wcx.flume.interceptor;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;

/**
 * 项目: flume
 * <p>
 * 功能描述:
 *
 * @author: WuChengXing
 * @create: 2020-12-16 09:01
 **/
public class JSONUtils {

    public static boolean isJSONValidate(String log) {
        try {
            JSON.parse(log);
            return true;
        } catch (JSONException e) {
            return false;
        }
    }

}
